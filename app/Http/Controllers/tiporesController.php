<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tiporesModel;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class tiporesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function __construct(){
        $this->beforeFilter('@find',['only'=>['edit','update','destroy']]);
    }*/
    public function index()
    {
        $data = tiporesModel::where('estado',true)->get();
        return view('tipores.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        tiporesModel::create($request->all());
        return redirect('tipores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = tiporesModel::find($id);
        return view('tipores.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = tiporesModel::find($id);
        return view('tipores.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipores = tiporesModel::find($id);
        $tipores -> update($request->all());
        return redirect('tipores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipores =  tiporesModel::where('id',$id)->update(['estado'=>false]);
        //tiporesModel::destroy($id);
        return redirect('tipores');
    }
}

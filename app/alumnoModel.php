<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumnoModel extends Model
{
  protected $fillable=['dni','nombre','appaterno','apmaterno','escuela','fechanacimiento','sexo','email'];

}

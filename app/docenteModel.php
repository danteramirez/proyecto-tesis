<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class docenteModel extends Model
{
    protected $fillable=['dni',
    					 'nombre',
    					 'ap_pat',
    					 'ap_mat',
    					 'direccion',
    					 'telefono',
    					 'titulo',
    					 'email',
    					 'sexo'
    					];
}

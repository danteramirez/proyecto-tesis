<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tiporesModel extends Model
{
    protected $fillable = ['name'];
}

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});



$factory->define(App\docenteModel::class, function (Faker\Generator $faker) {
    return [
    	'dni'=>str_random(8),
        'nombre' => $faker->name,
        'ap_pat' => $faker->lastName,
        'ap_mat' => $faker->lastName,
        'direccion' => $faker->address,
        'telefono' => str_random(10),
        'titulo' => $faker->city,
        'email' => $faker->email,
        'sexo' => 'F',
    ];
});

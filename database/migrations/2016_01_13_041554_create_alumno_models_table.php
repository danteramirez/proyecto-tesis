<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnoModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno_models', function (Blueprint $table) {
            $table->increments('id');
            $table->char('dni',8);
            $table->string('nombre',50);
            $table->string('appaterno',50);
            $table->string('apmaterno',50);
            $table->string('escuela',50);
            $table->date('fechanacimiento');
            $table->char('sexo',1);
            $table->string('email',50);
            $table->boolean('estado')->default(true);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alumno_models');
    }
}

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    editar <p>

    </p>
    <form class="" action="../../alumno/{{$data['id']}}" method="post">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="">
          <label for="">Dni:</label>
          <input type="text" name="dni" value="{{$data['dni']}}">

      </div>
      <div class="">
        <label for="">Nombres:</label>
        <input type="text" name="nombre" value="{{$data['nombre']}}">
      </div>
      <div class="">
        <label for="">Apellido Paterno:</label>
        <input type="text" name="appaterno" value="{{$data['appaterno']}}">
      </div>
      <div class="">
        <label for="">Apellido Materno:</label>
        <input type="text" name="apmaterno" value="{{$data['apmaterno']}}">
      </div>
      <div class="">
        <label for="">escuela</label>
        <input type="text" name="escuela" value="{{$data['escuela']}}">
      </div>
      <div class="">
        <label for="">Fecha Nacimiento</label>
        <input type="date" name="fechanacimiento" value="{{$data['fechanacimiento']}}">
      </div>
      <div class="">
        <label for="">Sexo</label>
        <select class="" name="sexo" values="{{$data['sexo']}}">

            @if($data['sexo']=='M')
            <option value="M">Masculino</option>
            <option value="F">Femenino</option>
            @else
            <option value="F">Femenino</option>
            <option value="M">Masculino</option>

        @endif
      </select>

      </div>
      <div class="">
        <label for="">email</label>
        <input type="text" name="email" value="{{$data['email']}}">

      </div>
    <div class="">
      <input type="submit" value="Editar">
    </div>
    </form>
  </body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<form action="../docente" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div>
			<label for="">Dni: </label>
			<input type="text" name="dni">
		</div>

		<div>
			<label for="">Nombre: </label>
			<input type="text" name="nombre">
		</div>

		<div>
			<label for="">Apellido Paterno :</label>
			<input type="text" name="ap_pat">
		</div>

		<div>
			<label for="">Apellido Materno :</label>
			<input type="text" name="ap_mat">
		</div>

		<div>
			<label for="">Dirección : </label>
			<input type="text" name="direccion">
		</div>

		<div>
			<label for="">Telefono :</label>
			<input type="text" name="telefono">
		</div>

		<div>
			<label for="">Titulo : </label>
			<input type="text" name="titulo">
		</div>

		<div>
			<label for="">Email</label>
			<input type="email" name="email">
		</div>
		
		<div>
			<select name="sexo" id="">
				<option value="M">M</option>
				<option value="F">F</option>
			</select>
		</div>
		<div>
			
			<input type="submit" value="Registrar Docente">
		</div>
	</form>
</body>
</html>
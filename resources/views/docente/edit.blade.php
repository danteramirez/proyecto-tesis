<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<form action="../../docente/{{ $data['id'] }}" method="post">
		<input name="_method" type="hidden" value="PUT">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div>
			<label for="">Dni: </label>
			<input type="text" name="dni" value="{{ $data['dni'] }}">
		</div>

		<div>
			<label for="">Nombre: </label>
			<input type="text" name="nombre" value="{{ $data['nombre'] }}">
		</div>

		<div>
			<label for="">Apellido Paterno :</label>
			<input type="text" name="ap_pat" value="{{ $data['ap_pat'] }}">
		</div>

		<div>
			<label for="">Apellido Materno :</label>
			<input type="text" name="ap_mat" value="{{ $data['ap_mat'] }}">
		</div>

		<div>
			<label for="">Dirección : </label>
			<input type="text" name="direccion" value="{{ $data['direccion'] }}">
		</div>

		<div>
			<label for="">Telefono :</label>
			<input type="text" name="telefono" value="{{ $data['telefono'] }}">
		</div>

		<div>
			<label for="">Titulo : </label>
			<input type="text" name="titulo" value="{{ $data['titulo'] }}">
		</div>

		<div>
			<label for="">Email</label>
			<input type="email" name="email" value="{{ $data['email'] }}">
		</div>
		
		<div>
			<select name="sexo" id="" value="{{ $data['nombre'] }}">
				@if($data['sexo']=='M')
					<option value="M">M</option>
					<option value="F">F</option>
				@else
					<option value="F">F</option>
					<option value="M">M</option>
				@endif
			</select>
		</div>
		<div>
			
			<input type="submit" value="Actualizar">
		</div>
	</form>
</body>
</html>
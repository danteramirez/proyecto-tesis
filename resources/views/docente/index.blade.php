<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

	{{-- CON BLADE --}}
	<table>
		<tr>
			<th>ID</th>
			<th>DNI</th>
			<th>NOMBRE</th>
			<th>APE. PATERNO</th>
			<th>APE. MATERNO</th>
			<th>DIRECCION</th>
			<th>TELEFONO</th>
			<th>TITULO</th>
			<th>EMAIL</th>
			<th>SEXO</th>
			<th>ACCIONES</th>
		</tr>
	@foreach($data as $docente)
		<tr>
			<td>{{ $docente['id']}}</td>
			<td>{{ $docente['dni']}}</td>
			<td>{{ $docente['nombre']}}</td>
			<td>{{ $docente['ap_pat']}}</td>
			<td>{{ $docente['ap_mat']}}</td>
			<td>{{ $docente['direccion']}}</td>
			<td>{{ $docente['telefono']}}</td>
			<td>{{ $docente['titulo']}}</td>
			<td>{{ $docente['email']}}</td>
			<td>{{ $docente['sexo']}}</td>
			<td><a href="docente/{{ $docente['id']}}/edit">Editar</a>
			<a href="docente/{{ $docente['id'] }}">Mostrar</a>
			
			<form action="docente/{{ $docente['id']}}" method="post">
				<input type="hidden" name="_method" value="DELETE">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="submit" value="eliminar">
			</form>
			</td>
		</tr>
	@endforeach
	</table>
</body>
</html>
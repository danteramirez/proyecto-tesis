<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Crear Tipo de Resolucion</title>
    </head>
    <body>
        <form action="../tipores" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="">Nombre</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" name="register" class="btn btn-primary" value="Registrar">
            </div>
        </form>
    </body>
</html>

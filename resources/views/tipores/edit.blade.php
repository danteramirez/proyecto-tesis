<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Crear Tipo de Resolucion</title>
    </head>
    <body>
        <form action="../../tipores/{{$data['id']}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <label for="">Nombre</label>
                <input type="text" name="name" class="form-control" value="{{$data['name']}}">
            </div>
            <div class="form-group">
                <input type="submit" name="editar" class="btn btn-primary" value="Editar">
            </div>
        </form>
    </body>
</html>

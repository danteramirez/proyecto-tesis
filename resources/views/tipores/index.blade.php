
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Tipo de Resolucion</title>
    </head>
    <body>

        <table border="1">
            <thead>
                <th>Nombre</th>
                <th colspan="2">
                    Operaciones
                </th>
            </thead>
            @foreach($data as $tipores)
            <tbody>
                <td>{{$tipores['name']}}</td>
                <td>
                    <a href="tipores/{{$tipores['id']}}/edit">Editar</a>
				    <a href="tipores/{{$tipores['id']}}">Mostrar</a>
                    <form action="tipores/{{$tipores['id']}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" name="delete" value="Eliminar">
                    </form>
                </td>
            </tbody>
            @endforeach
        </table>
    </body>
</html>

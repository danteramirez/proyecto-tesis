<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Mostrar Tipo Resolucion</title>
    </head>
    <body>
        <table border="1">
            <thead>
                <th>
                    ID
                </th>
                <th>
                    Nombre
                </th>
            </thead>
            <tbody>
                <td>
                    {{$data['id']}}
                </td>
                <td>
                    {{$data['name']}}
                </td>
            </tbody>
        </table>
    </body>
</html>
